//Admin Page Map js with hardcoded res data

var map;

function initialize() {
	
	//var json = JSON.parse($data);
	//console.log(json);
  
	//console.log('JSONObject Try. sampleKey value: ', sample.sampleKey);
	
	//console.log("Contents of residences from JSON : " + residences[0].Geolocation);
	//console.log("Now entering hardcoded marker values");
	
	var residences = [
                    ['SampleRes 1', 52.254327, -7.185161, 'Good Tenant'],
                    ['SampleRes 2', 53.352547,-6.2555785, 'Bad Tenant'],
                    ['SampleRes 3', 51.895984,-8.533089, 'Indifferent Tenant'],
                    ['SampleRes 4', 52.651494,-8.700179, 'Other Tenant'],
                    ['SampleRes 5', 53.283923,-9.188834, 'Strange Tenant']
                  ];
	
	//console.log("Now leaving hardcoded marker values");
	
	
  const center = new google.maps.LatLng(53.423239,-7.966524);

  const mapOptions = {
    center: center,
    zoom: 7,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  const mapDiv = document.getElementById('adminMap');

  map = new google.maps.Map(mapDiv, mapOptions);
  
  
  var infowindow = new google.maps.InfoWindow();

  var marker, i;
  
  for (i = 0; i < residences.length; i++) {  
  	
  	marker = new google.maps.Marker({
  		position: new google.maps.LatLng(residences[i][1], residences[i][2]),
  		//position: new google.maps.LatLng(json.Markers[0].Geolocation);
      map: map
    });
  	
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
    	return function() {
    		infowindow.setContent(residences[i][0] + ', ' + residences[i][3]);
    		//infowindow.setContent(residences[i][0] + ', ' + resMarkerList.Markers[0].Tenant);
        infowindow.open(map, marker);
      }
    })(marker, i));
    
  }
  

}

google.maps.event.addDomListener(window, 'load', initialize);