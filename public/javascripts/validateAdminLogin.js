$('.ui.form')
  .form({
    fields: {
    	'username': {
        identifier: 'username',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter your email'
          }
        ]
      },
      'password': {
        identifier: 'password',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter a password'
          }
        ]
      }
    }
  })
;