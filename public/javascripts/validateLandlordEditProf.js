$('.ui.form')
  .form({
    fields: {
    	'user.firstName': {
        identifier: 'user.firstName',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter your new first name'
          }
        ]
      },
      'user.lastName': {
        identifier: 'user.lastName',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter your new last name'
          }
        ]
      },
      'user.address': {
        identifier: 'user.address',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter a new home address'
          }
        ]
      }
    }
  })
;