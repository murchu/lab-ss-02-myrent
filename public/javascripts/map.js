/**
 * Created by iMurphu on 21/06/2016.
 */

function initialize()
{
  const latlng = new google.maps.LatLng(52.254427, -7.185281);

  const mapOptions =
  {
    center: new google.maps.LatLng(latlng.lat(), latlng.lng()),
    mapTypeId : google.maps.MapTypeId.SATELLITE,
    zoom : 16,
  };

  const map = new google.maps.Map(document.getElementById("mapcanvas"), mapOptions);

  //alternative way of using jquery to retrieve the mapcanvas element:
  //const map = new google.maps.Map($("mapcanvas").get(0), mapOptions);

  // place a marker
  const marker = new google.maps.Marker({
    map : map,
    draggable : true,
    position : latlng,
    title : "Drag and drop your property on screen",
  });

  // to add a marker to the map, call setMap()
  marker.setMap(map);

  //marker listener populates hidden fields on-dragend
  google.maps.event.addListener(marker, 'dragend', function() {
    const latLng = marker.getPosition();
    const latLong = latLng.lat().toString().substring(0,10) + ',' + latLng.lng().toString().substring(0,10);

    //publish lat long in geolocation control in html page
    $("#geolocation").val(latLong);

    //update the marker position
    map.setCenter(latLng);
  })
}

// draws map
google.maps.event.addDomListener(window, 'load', initialize);