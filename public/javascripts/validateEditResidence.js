$('.ui.form')
    .form({
      fields: {
        'residence.rentalAmount': {
          identifier: 'residence.rentalAmount',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter the rent value for your residence'
            }
          ]
        },
        'residence.numberBedrooms': {
          identifier: 'residence.numberBedrooms',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter the number of bedrooms'
            }
          ]
        },
        'residence.numberBathrooms': {
          identifier: 'residence.numberBathrooms',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter the number of bathrooms'
            }
          ]
        },
        'residence.area': {
          identifier: 'residence.area',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter the area of the residence'
            }
          ]
        },
        'residence.residenceType': {
          identifier: 'residence.residenceType',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter the type of the residence'
            }
          ]
        }
      }
    })
;