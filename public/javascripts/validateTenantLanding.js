$('.ui.form')
    .form({
      fields: {
        'eircode': {
          identifier: 'eircode',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please select the properties eircode from the dropdown'
            }
          ]
        }
      }
    })
;