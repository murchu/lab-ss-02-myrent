$('.ui.form')
    .form({
      fields: {
        'landlordId': {
          identifier: 'landlordId',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please select a landlord'
            }
          ]
        },
        'tenantId': {
          identifier: 'tenantId',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please select a tenant'
            }
          ]
        }
      }
    })
;