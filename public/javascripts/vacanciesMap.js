let circle;
function getCircleData() {
  const center = circle.getCenter();
  const latcenter = center.lat().toString();
  const lngcenter = center.lng().toString();
  var radius = circle.getRadius().toString();
  $('#radius').val(radius);
  $("#latcenter").val(latcenter);
  $("#lngcenter").val(lngcenter);
}

let map;
function initialize() {

  const center = new google.maps.LatLng(52.254427, -7.185281);

  const mapOptions = {
    center: center,
    zoom: 12,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  const mapDiv = document.getElementById('vacanciesReportMap');

  map = new google.maps.Map(mapDiv, mapOptions);
  const initRadius = 1000;
  circle = new google.maps.Circle({
    center: center,
    radius: initRadius,
    strokeColor: '#80bfff',
    strokeOpacity: 0.4,
    strokeWeight: 1,
    fillColor: '#80bfff',
    fillOpacity: 0.4,
    draggable: true
  });  

  circle.setEditable(true);
  circle.setMap(map);

}

google.maps.event.addDomListener(window, 'load', initialize);