$('.ui.form')
  .form({
    fields: {
    	'user.firstName': {
        identifier: 'user.firstName',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter your first name'
          }
        ]
      },
      'user.lastName': {
        identifier: 'user.lastName',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter your last name'
          }
        ]
      },
      'user.address': {
        identifier: 'user.address',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter your home address'
          }
        ]
      },
      'user.email': {
        identifier: 'user.email',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter your email'
          }
        ]
      },
      'user.password': {
        identifier: 'user.password',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter a password'
          }
        ]
      },
      'consentToTerms': {
        identifier: 'consentToTerms',
        rules: [
          {
            type   : 'checked',
            prompt : 'Please accept the terms to sign up'
          }
        ]
      }
    }
  })
;