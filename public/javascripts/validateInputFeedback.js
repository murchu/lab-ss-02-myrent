$('.ui.form')
    .form({
      fields: {
        'feedback.firstName': {
          identifier: 'feedback.firstName',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter your first name'
            }
          ]
        },
        'feedback.lastName': {
          identifier: 'feedback.lastName',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter your last name'
            }
          ]
        },
        'feedback.email': {
          identifier: 'feedback.email',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter your email'
            }
          ]
        },
        'feedback.message': {
          identifier: 'feedback.message',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter a message'
            }
          ]
        }
      }
    })
;