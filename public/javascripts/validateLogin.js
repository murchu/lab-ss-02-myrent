$('.ui.form')
  .form({
    fields: {
    	'user.email': {
        identifier: 'user.email',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter your email'
          }
        ]
      },
      'user.password': {
        identifier: 'user.password',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter a password'
          }
        ]
      }
    }
  })
;