import java.io.FileNotFoundException;

import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

/**
 *  @file					Bootstrap.java
 *  @description
 *		Runs before site loads and preloads data
 *		
 *  @author				Damien Murphy
 *  @since				22 Jul 2016
 */
@OnApplicationStart
public class Bootstrap extends Job 
{
  
  public void doJob() throws FileNotFoundException
  {
    Fixtures.deleteDatabase();
    //Fixtures.loadModels("data.yml");
    Fixtures.loadModels("data2.yml");
  }
  
}