package controllers;

import java.util.Date;

import models.Residence;
import play.Logger;
import play.mvc.Controller;

/**
 *  @file					InputData.java
 *  @description
 *		Provides the functionality for users to manage properties added to the site
 *		
 *  @author				Damien Murphy
 *  @since				22 Jul 2016
 */
public class InputData extends Controller
{
  /**
   * Renders the default view that allows users to add a new property to the site 
   */
  public static void inputdata()
  {
    render();
  }
  
  /**
   * Adds a new property (residence) to the site db.
   * Time-date stamps the registration date as the current time-date, and saves
   * the property to the db. 
   * Redirects to the inputdata to allow additional properties to be added.
   * 
   * @param residence
   */
  public static void registerProperty(Residence residence)
  {
    residence.owner = Landlords.loggedInUser();
    residence.dateRegistered = new Date();
    Logger.info("Registering property: " + residence);
    residence.save();
    inputdata();
  }
}
