package controllers;

import java.util.Date;

import models.Feedback;
import play.Logger;
import play.mvc.Controller;

/**
 *  @file					Contact.java
 *  @description
 *		Manages feedback-related functionality
 *		
 *  @author				Damien Murphy
 *  @since				22 Jul 2016
 */
public class Contact extends Controller
{
  /**
   * Renders inputfeedback view to allow users to submit feedback 
   */
  public static void inputfeedback()
  {
    render();
  }
  
  /**
   * Renders the feedbackconfirmation view to confirm receipt of feedback
   */
  public static void feedbackconfirmation()
  {
    render();
  }
  
  /**
   * Processes the feedback received from a user. 
   * Time-date stamps and saves the feedback object to the db.
   * 
   * @param feedback the feedback data received from the user
   */
  public static void submitfeedback(Feedback feedback)
  {
    feedback.dateReceived = new Date();
    feedback.save();
    feedbackconfirmation();
  }  
  
}
