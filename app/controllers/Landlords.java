package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Landlord;
import models.Residence;
import play.Logger;
import play.mvc.Controller;

/**
 *  @file         Landlords.java
 *  @description
 *    Controls the funtionality around landlord users on the site.
 *    
 *  @author       Damien Murphy
 *  @since        22 Jul 2016
 */
public class Landlords extends Controller
{
  /**
   * Renders the signin view for users to sign in
   */
  public static void signin()
  {
    render();
  }
  
  /**
   * Renders the signin view for users to sign up 
   */
  public static void signup()
  {
    render();
  }
  
  /**
   * Renders the config view for landlords with their details and properties
   */
  public static void config()
  {
    Landlord landlord = loggedInUser();
    List<Residence> allResidences = Residence.findAll();
    List<Residence> landlordResidences = new ArrayList<Residence>();
    
    for (Residence res : allResidences)
    {
      if (res.owner.id == landlord.id)
        landlordResidences.add(res);
    }
    
    render(landlord, landlordResidences);
  } 
  
  /**
   * Renders the editResidence view to amend the residence in question
   * 
   * @param id id of the residence to be amended
   */
  public static void editResidence(String eircode)
  {
    Logger.info("eircode received by editResidence() : " + eircode);
    
    Logger.info("Finding Residence with Eircode: " + eircode);
    
    Residence residence = Residence.find("eircode", eircode).first();
    
    Logger.info("Found Residence: " + residence);
    
    render(residence);    
  }
  
  /**
   * Renders the editProfile view for users to change their details 
   */
  public static void editProfile()
  {
    Landlord landlord = Landlords.loggedInUser();
    render(landlord);
  }
  
  /**
   * Adds a new landlord user to the site db.
   * User must agree to site terms to be registered, otherwise site redirects to 
   * signup page.
   * 
   * @param user user to be added
   * @param consentToTerms boolean representing consent to site terms
   */
  public static void register(Landlord user, boolean consentToTerms)
  {
    if (consentToTerms)
    {
      Logger.info("Registering new user. First Name: " + user.firstName +
                    ". Last Name: " + user.lastName + ". Email: " + user.email +
                    ". Password: " + user.password);
      user.save();
      signin();
    }
    
    else
    {
      signup();
    }
  }
  
  /**
   * Edits the details of the currently logged in landlord user.
   * Landlord firstName, lastName, and address are changed to those submitted, 
   * the amended landlord user is saved to the db, and the user redirected to 
   * the inputData view.
   * 
   * @param user user object with the relevant details that needs to be changed
   */
  public static void editDetails(Landlord user)
  {
    Landlord landlord = Landlords.loggedInUser();
    
    if (!landlord.firstName.equals(user.firstName) && !user.firstName.isEmpty())
      landlord.firstName = user.firstName;
    
    if (!landlord.lastName.equals(user.lastName) && !user.lastName.isEmpty())
      landlord.lastName = user.lastName;
    
    if (!landlord.address.equals(user.address) && !user.address.isEmpty())
      landlord.address = user.address;

    landlord.save();
    
    InputData.inputdata();
    
  }
  
  /**
   * Edits the details of a residence.
   * Amended residence is saved to the db.
   * 
   * @param id id of the residence to be edited
   */
  public static void editResidenceDetails(Residence residence)
  {
    Residence dbRes = Residence.find("eircode", residence.eircode).first();
    
    if (dbRes.geoLocation != residence.geoLocation)
      dbRes.geoLocation = residence.geoLocation;
        
    if (dbRes.numberBedrooms != residence.numberBedrooms)
      dbRes.numberBedrooms = residence.numberBedrooms;
    
    if (dbRes.numberBathrooms != residence.numberBathrooms)
      dbRes.numberBathrooms = residence.numberBathrooms;
    
    if (dbRes.residenceType != residence.residenceType)
      dbRes.residenceType = residence.residenceType;
    
    if (dbRes.area != residence.area)
      dbRes.area = residence.area;
        
    Logger.info("Saving new residence details: " + dbRes);
    dbRes.save();
    
    config();
  }
  
  /**
   * Deletes a residence from the site and DB
   *
   */
  public static void deleteResidence(String eircode)
  {
    Logger.info("Eircode received by deleteResidence() " + eircode);
    
    Residence residence = Residence.find("eircode", eircode).first();
    
    Landlord landlord = Landlords.loggedInUser();
    
    // remove residence relationship between residence and landlord
    Logger.info("Removing residence from landlords list of properties: "+ residence);    
    Logger.info("No. of landlords properties before removal: " + landlord.residences.size());
    residence.owner = null;
    Landlords.loggedInUser().residences.remove(residence);
    Logger.info("No. of landlords properties after removal: " + landlord.residences.size());
    
    
    if (residence.tenant == null)
    {
      Logger.info("No tenant attached to property - deleting property from DB also: " + residence);
      
      residence.delete();
      //Residence.delete("eircode", eircode);
      
      Logger.info("Deleted residence: " + residence);
    }
    
    config();
  }
  
  /**
   * Verifies a user is a valid landlord user of the site.
   * 
   * @param user user to be validated
   */
  public static void authenticate(Landlord user)
  {
    Logger.info("Trying to authenticate user: " + user.email + ". Password: " + user.password);
    
    String password = user.password;    
    
    try
    {
      user = Landlord.findByEmail(user.email);
      
      if ((user != null) && (user.password.equals(password)))
      {
        Logger.info("Authentication Successful");
        session.put("logged_in_landlordId", user.id);
        
        InputData.inputdata();
      }
      
      else
      {
        Logger.info("Authentication Failed");
        Landlords.signup();
      } 
    }
    catch (NullPointerException e)
    {
      Logger.info("Caught NullPointer Exception, redirected to signup page");
      Landlords.signup();
    }
  }
  
  /**
   * Logs the user out from the site.
   * Clears the users session cookie, and redirects user to frontpage landing view 
   */
  public static void signout()
  {
    session.remove("logged_in_landlordId");
    Home.welcome();
  }
  
  /**
   * Returns the landlord user currently logged into the site
   * 
   * @return the landlord user currently logged in
   */
  public static Landlord loggedInUser()
  {
    String userId = session.get("logged_in_landlordId");
    Landlord user = Landlord.findById(Long.parseLong(userId));
    return user;    
  }
}
