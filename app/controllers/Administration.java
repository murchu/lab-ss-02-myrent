package controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Persistence;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import models.Landlord;
import models.Residence;
import models.Tenant;
import play.Logger;
import play.mvc.Controller;
import utils.ResEircodeComparator;
import utils.ResLandlordComparator;
import utils.ResRentComparator;
import utils.ResStatusComparator;
import utils.ResTypeComparator;

/**
 *  @file					Administration.java
 *  @description
 *		Controls the admin-related functionality
 *		
 *  @author				Damien Murphy
 *  @since				8 Aug 2016
 */
public class Administration extends Controller
{
  public static void signin()
  {
    render();
  }
  
  public static void landing()
  {
    List<Landlord> allLandlords = Landlord.findAll();
    List<Tenant> allTenants = Tenant.findAll();
    
    List<Residence> allResidences = Residence.findAll();
        
    JSONObject resMarkerList = new JSONObject();
    JSONArray jsonArray = new JSONArray();
    for (Residence res : allResidences)
    {
      JSONObject resDetails = new JSONObject();      
      
      resDetails.put("Eircode", res.eircode);
      resDetails.put("Geolocation", res.geoLocation);
      if (res.tenant == null)
      {
        resDetails.put("Tenant", "Unoccupied");
      }
      else
      {
        resDetails.put("Tenant", res.tenant.firstName);
      }
      
      jsonArray.add(resDetails);
    }
    
    resMarkerList.put("Markers", jsonArray);    
    
    Logger.info("Passing JSON Array of Res. Markers: " + resMarkerList);
    
    //JSONObject sample = new JSONObject();
    //sample.put("sampleKey", "sampleValue");
    
    render(allLandlords, allTenants, jsonArray);
    
  }
    
  public static void adminReport(String filter)
  {
    List<Residence> allRes = Residence.findAll();
    
    switch (filter) {
      case "default":
        Collections.sort(allRes, new ResEircodeComparator());
        break;
      case "defaultrev":
        Collections.sort(allRes, new ResEircodeComparator());
        Collections.reverse(allRes);
        break;
      case "byrented":
        Collections.sort(allRes, new ResStatusComparator());         
        break;
      case "byrentedrev":
        Collections.sort(allRes, new ResStatusComparator());  
        Collections.reverse(allRes);
        //Collections.sort(allRes, Collections.reverseOrder(new ResStatusComparator()));
        break;
      case "bytype":
        Collections.sort(allRes, new ResTypeComparator());
        break;
      case "bytyperev":
        Collections.sort(allRes, new ResTypeComparator());
        Collections.reverse(allRes);
        break;
      case "byrent":
        Collections.sort(allRes, new ResRentComparator());
        break;
      case "byrentrev":
        Collections.sort(allRes, new ResRentComparator());
        Collections.reverse(allRes);
        break;
      default: 
        break;
    }
       
    render(allRes);
  }

  public static void adminChart()
  {
    List<Residence> allRes = Residence.findAll();
    Collections.sort(allRes, new ResLandlordComparator());
        
    render(allRes);
  }
  
  public static void getChartData()
  {                     
    List<Landlord> landlords = activeLandlords();
    
    List<List<String>> residences = new ArrayList<List<String>>();
    
    //Logger.info("Generating chart data now...");
    for (Landlord ll : landlords)
    {
      String name = new String(ll.firstName + " " + ll.lastName);
      String marketShare = new String(String.valueOf(shareOfMarket(ll)));
      //Logger.info("LL: " + name + ".MarketShare: " + marketShare);
      residences.add(landlords.indexOf(ll), Arrays.asList(name, marketShare));
    }
    
    renderJSON(residences);    
  }
  
  public static int totalRentalIncome(Landlord landlord)
  {
    List<Residence> allRes = Residence.findAll();
    
    int totalIncome = 0;
    
    for (Residence res : allRes)
    {
      if (res.owner.id == landlord.id)
      {
        totalIncome += res.rentalAmount;
      }
    }
    
    return totalIncome;
  }
  
  public static int totalRentalMarket()
  {
    List<Residence> allRes = Residence.findAll();
    
    int totalMarket = 0;
    
    for (Residence res : allRes)
    {
      totalMarket += res.rentalAmount;
    }
    
    return totalMarket;
  }
  
  public static double shareOfMarket(Landlord landlord)
  {    
    return (totalRentalIncome(landlord) * 100.0)/ totalRentalMarket();
  }
  
  public static ArrayList<Landlord> activeLandlords()
  {
    HashMap<Long, Landlord> landlords = new HashMap<Long, Landlord>(); 
    
    List<Residence> allRes = Residence.findAll();

    for (Residence res : allRes)
    {
      
      landlords.put(res.owner.id, res.owner);
    }
    
    return new ArrayList<Landlord>(landlords.values());
  }
  
  public static void listGeolocations()
  {
    List<Residence> allRes = Residence.findAll();
    
    List<List<String>> markerList = new ArrayList<List<String>>();    
    
    for (Residence res : allRes)
    {
      String eircode = new String(res.eircode);
                  
      String[] geoLoc = res.geoLocation.split("\\s*,\\s*");
      String lat = new String(geoLoc[0]);
      String lng = new String(geoLoc[1]);
      
      String tenant;
      if (res.tenant == null)
      {
        tenant = new String("Currently Unoccupied");
      }
      else
      {
        tenant = new String(res.tenant.firstName + " " + res.tenant.lastName);
      }
      
      markerList.add(allRes.indexOf(res), Arrays.asList(eircode, lat, lng, tenant));
    }
          
    renderJSON(markerList);    
  }
  
  public static void authenticate(String username, String password)
  {
    if ((username.equals("admin@witpress.ie")) && (password.equals("secret")))
    {
      session.put("loggedInAdmin", "admin@witpress.ie");
      landing();
    }
    else
    {
      Home.welcome();
    }      
  }
  
  public static void deleteLandlord(Long landlordId)
  {        
    Landlord landlord = Landlord.findById(landlordId);    
    
    // break any relationships with res's
    List<Residence> allResidences = Residence.findAll();
    for (Residence res : allResidences)
    {
      if (res.owner.id == landlord.id)
      {                
        landlord.residences.remove(res);
        landlord.save();
        res.owner = null;
        res.save();
      }
    }
        
    landlord.delete();
    
    landing();
  }
  
  public static void deleteTenant(Long tenantId)
  {
    Tenant tenant = Tenant.findById(tenantId);
    Logger.info("Found tenant: " + tenant);
    
    // break relationship with any res
    List<Residence> allResidences = Residence.findAll();
    for (Residence res : allResidences)
    {
      Logger.info("res: " + res);
      Logger.info("tenant: " + tenant);
      if ((res.tenant != null) && (res.tenant.id == tenant.id))
      {
        tenant.tenancy = null;
        tenant.save();
        res.tenant = null;
        res.save();
      }
    }
    
    tenant.delete();
    
    landing();
  }
  
  public static void logout()
  {
    session.remove("loggedInAdmin");
    Home.welcome();
  }
}
