package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Residence;
import models.Landlord;
import play.mvc.Controller;
import utils.Circle;
import utils.Geodistance;
import utils.LatLng;

/**
 *  @file					ReportController.java
 *  @description
 *		Controls the functionality surrounding the selection and generation of 
 *    reports on properties registered on the site.
 *		
 *  @author				Damien Murphy
 *  @since				22 Jul 2016
 */
public class ReportController extends Controller
{
  /**
   * Renders the selectRegion view to allow users to select the geographic 
   * region they wish to report on. 
   */
  public static void selectRegion()
  {
    render();
  }
  
  /**
   * Renders the reportData view which presents data on all properties in the 
   * region the user selected. 
   */
  public static void reportData()
  {
    render();
  }
  
  /**
   * Takes the data from the region the user selected on the map, locates all 
   * registered properties in that region, and sends all details of those 
   * properties to the reportData view to be rendered on screen.
   * 
   * @param radius radius of the circle
   * @param latcenter latitude of the center point of the selected circle
   * @param lngcenter longtitude of the center point of the selected circle
   */
  public static void generateReport(double radius, double latcenter, double lngcenter)
  {
    Circle circle = new Circle(latcenter, lngcenter, radius);
    
    Landlord user = Landlords.loggedInUser();
    
    List<Residence> residences = new ArrayList<Residence>();
    
    List<Residence> allResidences = Residence.findAll();
    for (Residence residence : allResidences)
    {
      LatLng residenceLocation = residence.getGeolocation();
      if (Geodistance.inCircle(residenceLocation, circle))
      {
        residences.add(residence);
      }
    }
    
    render("ReportController/reportData.html", user, circle, residences);    
  }

}
