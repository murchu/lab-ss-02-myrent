package controllers;

import play.mvc.Controller;

/**
 *  @file         Home.java
 *  @description
 *    Controls the site landing page functions for users accessing the site
 *    
 *  @author       Damien Murphy
 *  @since        22 Jul 2016
 */
public class Home extends Controller
{
  /**
   * Renders the default frontpage/ landing-page view 
   */
  public static void welcome()
  {
    render();
  }
}
