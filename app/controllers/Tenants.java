package controllers;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import models.Landlord;
import models.Residence;
import models.Tenant;
import play.Logger;
import play.mvc.Controller;
import utils.Circle;
import utils.Geodistance;
import utils.LatLng;

/**
 *  @file         Tenants.java
 *  @description
 *    Controls the funtionality around tenant users on the site.
 *    
 *  @author       Damien Murphy
 *  @since        26 Jul 2016
 */
public class Tenants extends Controller
{
  /**
   * Renders the signin view for users to sign in
   */
  public static void signin()
  {
    render();
  }
  
  /**
   * Renders the signin view for users to sign up 
   */
  public static void signup()
  {
    render();
  }
  
  /**
   * Renders the editProfile view for users to change their details 
   */
  public static void editProfile()
  {
    Tenant tenant = Tenants.loggedInUser();
    render(tenant);
  }
  
  public static void tenantLanding()
  {
    Tenant tenant = loggedInUser();
    
    Logger.info("Searching for unrented properties...");
    ArrayList<Residence> unrentedProperties = unrentedProperties();
    Logger.info("Number of unrented properties: " + unrentedProperties.size());
    
    //Logger.info("Rendering Tenant: " + tenant);
    Logger.info("Rendering List of Available Properties: " + unrentedProperties);
    render(tenant, unrentedProperties);
  }
  
  public static void vacanciesReport(double radius, double latcenter, double lngcenter)
  {
    Logger.info("Entering vacanciesReport()");
    
    Circle circle = new Circle(latcenter, lngcenter, radius);
    
    Tenant user = Tenants.loggedInUser();
    
    List<Residence> availableResidences = new ArrayList<Residence>();
    
    List<Residence> allUnrentedResidences = unrentedProperties();
    for (Residence residence : allUnrentedResidences)
    {
      LatLng residenceLocation = residence.getGeolocation();
      if ( Geodistance.inCircle(residenceLocation, circle) )
      {
        availableResidences.add(residence);
      }
    }
    
    render("Tenants/vacanciesReport.html", user, circle, availableResidences);
    
    Logger.info("Leaving vacanciesReport()");
  }
  
  /**
   * Adds a new tenant user to the site db.
   * User must agree to site terms to be registered, otherwise site redirects to 
   * signup page.
   * 
   * @param user user to be added
   * @param consentToTerms boolean representing consent to site terms
   */
  public static void register(Tenant user, boolean consentToTerms)
  {
    if (consentToTerms)
    {
      Logger.info("Registering new user. First Name: " + user.firstName +
                    ". Last Name: " + user.lastName + ". Email: " + user.email +
                    ". Password: " + user.password);
      user.save();
      signin();
    }
    
    else
    {
      signup();
    }
  }
  
  /**
   * Edits the details of the currently logged in tenant user.
   * Tenant firstName, lastName, and address are changed to those submitted, 
   * the amended tenant user is saved to the db, and the user redirected to 
   * the inputData view.
   * 
   * @param user user object with the relevant details that needs to be changed
   */
  public static void editDetails(Tenant user)
  {
    Tenant tenant = Tenants.loggedInUser();
    
    if (!tenant.firstName.equals(user.firstName) && !user.firstName.isEmpty())
      tenant.firstName = user.firstName;
    
    if (!tenant.lastName.equals(user.lastName) && !user.lastName.isEmpty())
      tenant.lastName = user.lastName;
    
    tenant.save();
        
    Tenants.tenantLanding();
    
  }
  
  /**
   * Verifies a user is a valid tenant user of the site.
   * 
   * @param user user to be validated
   */
  public static void authenticate(Tenant user)
  {
    Logger.info("Trying to authenticate user: " + user.email + ". Password: " + user.password);
    
    String password = user.password;    
    
    try
    {
      user = Tenant.findByEmail(user.email);
      
      if ((user != null) && (user.password.equals(password)))
      {
        Logger.info("Authentication Successful");
        session.put("logged_in_tenantId", user.id);
        
        Tenants.tenantLanding();
      }
      
      else
      {
        Logger.info("Authentication Failed");
        Tenants.signup();
      } 
    }
    catch (NullPointerException e)
    {
      Logger.info("Caught NullPointer Exception, redirecting to signup page");
      Tenants.signup();
    }
    
  }
  
  /**
   * Removes the tenants current tenancy and renders the tenantLanding view
   * 
   */
  public static void registerTenancy(String eircode)
  {
    Tenant tenant = loggedInUser();
    tenant.tenancy = Residence.find("eircode", eircode).first();
    tenant.save();

    tenantLanding();
  }
  
  /**
   * Removes the tenants current tenancy and renders the tenantLanding view
   */
  public static void removeTenancy()
  {
    Tenant tenant = loggedInUser();
    Logger.info("Removing tenants tenancy....");
    Logger.info("Current Tenancy: " + tenant.tenancy.eircode);
    tenant.tenancy = null;
    tenant.save();
    tenantLanding();
  }
  
  /**
   * Logs the user out from the site.
   * Clears the users session cookie, and redirects user to frontpage landing view 
   */
  public static void signout()
  {
    session.remove("logged_in_tenantId");
    Home.welcome();
  }
  
  /**
   * Returns the tenant user currently logged into the site
   * 
   * @return the tenant user currently logged in
   */
  public static Tenant loggedInUser()
  {
    String userId = session.get("logged_in_tenantId");
    Tenant user = Tenant.findById(Long.parseLong(userId));
    return user;    
  }
  
  public static ArrayList<Residence> unrentedProperties()
  {
    List<Residence> allResidences = Residence.findAll();                
    ArrayList<Residence> availableResidences = new ArrayList<Residence>();
    for (Residence res : allResidences) 
    {
      if (res.tenant == null)
      {
        availableResidences.add(res);
      }
    }
    
    return availableResidences;
  }
}
