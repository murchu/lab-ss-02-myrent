package utils;

import java.util.Comparator;

import models.Residence;

/**
 *  @file					ResRentComparator.java
 *  @description
 *		Comparator class that facilitates the comparison of rental rates of 
 *    two Residence objects
 *		
 *  @author				Damien Murphy
 *  @since				17 May 2016
 *  @version 			1.0
 */
public class ResRentComparator implements Comparator<Residence>
{

  /**
   * Compares two integer values
   * 
   * @return 0 if b is equal to a, 
   *          less than zero if b is less than a, and 
   *          greater than zero if b is greater than a
   * 
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  @Override
  public int compare(Residence a, Residence b)
  {
    Integer rentResA = new Integer(a.rentalAmount);
    
    return rentResA.compareTo(b.rentalAmount);
  }
}