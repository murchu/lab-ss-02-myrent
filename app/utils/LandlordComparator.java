package utils;

import java.util.Comparator;

import models.Landlord;

/**
 * A comparator class that facilitates comparison between the concatenated first
 * and second names of 2 Landlord objects.
 * Example: Landlord l1 has String firstName and String lastName. 
 * The comparison is between lastName+firstName in both messages' originators.
 * 
 */
public class LandlordComparator implements Comparator<Landlord>
{

  /**
   * Performs a lexicographic comparison the String name fields of two Tenant objects
   * 
   * @param l1
   *          the first tenant object
   * @param l2
   *          the second tenant object
   * 
   * @return 0 if the user name in l1 is equal to the name in l2 less than zero
   *         if name in l1 less than name in l2 greater than zero if name in l2
   *         greater than name in l2
   */
  @Override
  public int compare(Landlord l1, Landlord l2)
  {
    String firstUser = l1.lastName + l1.firstName;
    String secondUser = l2.lastName + l2.firstName;
    
    return firstUser.compareToIgnoreCase(secondUser);
  }

}