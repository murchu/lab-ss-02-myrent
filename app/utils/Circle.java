package utils;

/**
 *  @file         Circle.java
 *  @description
 *    Circle utility class to represent a circle object on a map
 *    
 *  @author       Damien Murphy
 *  @since        22 Jul 2016
 */
public class Circle
{
  public double radius;
  public double latcenter;
  public double lngcenter;
  
  /**
   * Constructs a new circle object from the passed latitude, longtitude and 
   * radius values
   * 
   * @param lat latitude of the center point of the circle
   * @param lng longtitude of the center point of the circle
   * @param radius
   */
  public Circle(double lat, double lng, double radius) 
  {
    this.radius = radius;
    this.latcenter = lat;
    this.lngcenter = lng;
  }
  
  /**
   * Returns a LatLng object representing the geo co-ordinates of the center of 
   * the circle
   * 
   * @return a LatLng object representing the geo co-ordinates of the center of 
   * the circle
   */
  public LatLng getCenter() 
  {
    return new LatLng(latcenter,lngcenter);
  }
  
  /**
   * Returns the radius of the circle
   * 
   * @return the radius of the circle
   */
  public double getRadius() 
  {
    return radius;
  }
}
