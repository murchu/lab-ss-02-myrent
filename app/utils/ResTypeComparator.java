package utils;

import java.util.Comparator;

import models.Residence;

/**
 * A comparator class that facilitates comparison 2 string objects.
 * Example: String r1 has String firstName and String lastName. 
 * The comparison is between lastName+firstName in both messages' originators.
 * 
 */
public class ResTypeComparator implements Comparator<Residence>
{

  /**
   * Performs a lexicographic comparison of two String objects
   * 
   * @param r1
   *          the first string object
   * @param r2
   *          the second string object
   * 
   * @return 0 if r1 is equal to r2 less than zero
   *         if r1 less than r2 greater than zero if r2
   *         greater than r2
   */
  @Override
  public int compare(Residence r1, Residence r2)
  {    
    return r1.residenceType.compareToIgnoreCase(r2.residenceType);
  }

}