package utils;

import java.util.Comparator;
import java.util.Date;

/**
 *  @file					DateComparator.java
 *  @description
 *    A comparator class that facilitates comparison between two Date objects 
 *	
 *  @author				Damien Murphy
 *  @since				16 Aug 2016
 *  @version 			1.0
 */
public class DateComparator implements Comparator<Date>
{
  /**
   * Compares two date objects
   * 
   * @param a the first Date object
   * @param b the second Date comparison object
   * 
   * @return an int value of 0, <0, or >0 representing whether the date o1 was 
   * 					posted at is equal to, less than, or greater than o2
   * 
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  @Override
  public int compare(Date a, Date b)
  {
    return a.compareTo(b);
  }

}
