package utils;

/**
 *  @file         LatLng.java
 *  @description
 *    LatLng utility class to represent the latitude and longtitude of a point 
 *    on a map.
 *    
 *  @author       Damien Murphy
 *  @since        22 Jul 2016
 */
public class LatLng
{

  private double lat;
  private double lng;

  /**
   * Constructs a LatLng object from the supplied latitude and longtitude values
   * 
   * @param lat
   * @param lng
   */
  public LatLng(double lat, double lng)
  {
    this.lat = lat;
    this.lng = lng;
  }

  public String toString()
  {
    return lat + "," + lng;
  }

  /**
   * @param latlng
   *          : a string comprising a lat,lng : eg 53.444,-5.455
   * @return a LatLng object whose fields obtained by parsing argument
   */
  public static LatLng toLatLng(String latlng)
  {
    String[] latLng = latlng.split(",");
    return new LatLng(Double.parseDouble(latLng[0]), Double.parseDouble(latLng[1]));
  }


  /**
   * Returns the latitude value of the LatLng object
   * 
   * @return the latitude value of the LatLng object
   */
  public double getLatitude()
  {
    return lat;
  }

  /**
   * Returns the longtitude value of the LatLng object
   * 
   * @return the longtitude value of the LatLng object
   */
  public double getLongitude()
  {
    return lng;
  }

}