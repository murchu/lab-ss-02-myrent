package utils;

import java.util.Comparator;

import models.Residence;

/**
 * A comparator class that facilitates comparison between the concatenated first
 * and second names of 2 landlord objects of residences
 * The comparison is between lastName+firstName
 * 
 */
public class ResLandlordComparator implements Comparator<Residence>
{

  /**
   * Performs a lexicographic comparison the String name fields of two landlord objects
   * 
   * @param r1
   *          the first residence object
   * @param r2
   *          the second residence object
   * 
   * @return 0 if the user name in r1 is equal to the name in r2 less than zero
   *         if name in r1 less than name in r2 greater than zero if name in r2
   *         greater than name in r2
   */
  @Override
  public int compare(Residence r1, Residence r2)
  {
    String firstOwner = r1.owner.lastName + r1.owner.firstName;
    String secondOwner = r2.owner.lastName + r2.owner.firstName;

    return firstOwner.compareToIgnoreCase(secondOwner);
  }

}