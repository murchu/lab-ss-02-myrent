package utils;

import java.util.Comparator;

import models.Residence;

/**
 * A comparator class that facilitates comparison 2 residence objects on the basis
 * of the residence tenants string names 
 */
public class ResStatusComparator implements Comparator<Residence>
{

  /**
   * Performs a lexicographic comparison of two String objects
   * 
   * @param r1
   *          the first string object
   * @param r2
   *          the second string object
   * 
   * @return 0 
   *          if r1.tenant and r2.tenant are both null
   *          if r1.tenant and r2.tenant are both equal
   *         -1
   *          if r1.tenant is not null and r2.tenant is null
   *          if r1.tenant is less than r2.tenant
   *         1
   *          if r1.tenant is null and r2.tenant is not null
   *          if r1.tenant is greater than r2.tenant
   */
  @Override
  public int compare(Residence r1, Residence r2)
  {    
    //return r1.residenceType.compareToIgnoreCase(r2.residenceType);
    if ((r1.tenant == null) && (r2.tenant == null))
      return 0;
    else if ((r1.tenant != null) && (r2.tenant == null))
      return -1;
    else if ((r1.tenant == null) && (r2.tenant != null))
      return 1;
    else
    {
      String firstTenant = r1.tenant.lastName + r1.tenant.firstName;
      String secondTenant = r2.tenant.lastName + r2.tenant.firstName;      
      return firstTenant.compareToIgnoreCase(secondTenant);
    }
  }

}