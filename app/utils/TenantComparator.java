package utils;

import java.util.Comparator;

import models.Tenant;

/**
 * A comparator class that facilitates comparison between the concatenated first
 * and second names of 2 tenant objects.
 * Example: Tenant t1 has String firstName and String lastName. 
 * The comparison is between lastName+firstName in both messages' originators.
 * 
 */
public class TenantComparator implements Comparator<Tenant>
{

  /**
   * Performs a lexicographic comparison the String name fields of two Tenant objects
   * 
   * @param t1
   *          the first tenant object
   * @param t2
   *          the second tenant object
   * 
   * @return 0 if the user name in t1 is equal to the name in t2 less than zero
   *         if name in t1 less than name in t2 greater than zero if name in t2
   *         greater than name in t2
   */
  @Override
  public int compare(Tenant t1, Tenant t2)
  {
    String firstUser = t1.lastName + t1.firstName;
    String secondUser = t2.lastName + t2.firstName;
    
    return firstUser.compareToIgnoreCase(secondUser);
  }

}