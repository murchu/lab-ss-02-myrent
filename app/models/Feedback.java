package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import play.db.jpa.Model;

/**
 *  @file					Feedback.java
 *  @description
 *		Feedback db model to represent feedback received from users.
 *		
 *  @author				Damien Murphy
 *  @since				22 Jul 2016
 */
@Entity
public class Feedback extends Model
{
  public Date dateReceived;
  public String firstName;
  public String lastName;
  public String email;
  
  @Lob
  public String message;
  
}
