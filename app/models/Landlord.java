package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import play.Logger;
import play.db.jpa.Model;

/**
 *  @file         Landlord.java
 *  @description
 *    Landlord db model representing landlord users registered with the site.
 *    
 *  @author       Damien Murphy
 *  @since        22 Jul 2016
 */
@Entity
public class Landlord extends Model
{  
  public String firstName;
  public String lastName;
  public String address;
  public String email;
  public String password;
  
  @OneToMany(mappedBy = "owner")  
  public List<Residence> residences = new ArrayList<Residence>();  
  
  /**
   * Returns the landlord user that corresponds to the email submitted
   * 
   * @param email email of the landlord to search for
   * @return the landlord that corresponds to the email passed
   */
  public static Landlord findByEmail(String email)
  {
    return find("email", email).first();
  }
  
  /**
   * Registers a residence as the landlords property
   * 
   * @param residence residence to be registered as belonging to the landlord
   */
  public void registerProperty(Residence residence)
  {
    if (!isRegistered(residence))
    {
      residences.add(residence);
    }
    else
    {
      Logger.info("Residence: " + residence + " is already registered by " + this.firstName);
    }
  }
  
  
  /**
   * Removes a property from the residences registered to the landlord
   * 
   * @param residence residence to be removed
   */
  public void removeProperty(Residence residence)
  {
    if (!isRegistered(residence))
    {
      residences.remove(residence);
    }
    else
    {
      Logger.info("Residence: " + residence + " is not a property of " + this.firstName);
    }
  }
  
  /**
   * Checks if the residence is already registered to the landlord
   * 
   * @param residence
   * @return
   */
  private boolean isRegistered(Residence residence)
  {
    return residences.contains(residence);
  }

  @Override
  public String toString()
  {
    return "Landlord [firstName=" + firstName + ", lastName=" + lastName + ", address=" + address + ", email=" + email
        + ", password=" + password + "]";
  }  
  
}
