package models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import play.db.jpa.Model;
import utils.LatLng;

/**
 *  @file					Residence.java
 *  @description
 *		Residence db model to represent residences registered with the site.
 *		
 *  @author				Damien Murphy
 *  @since				22 Jul 2016
 */
@Entity
public class Residence extends Model
{
  @ManyToOne(cascade = CascadeType.ALL)
  public Landlord owner;
    
  @OneToOne
  public Tenant tenant;
  
  public String geoLocation;
  public String eircode;
  
  public int rentalAmount;
  
  public int numberBedrooms;
  public int numberBathrooms;
  public String residenceType;
  
  public int area;
  
  public Date dateRegistered;
  
  /**
   * Returns a LatLng object representing the geolocation of the residence
   * 
   * @return a LatLng object representing the geolocation of the residence
   */
  public LatLng getGeolocation()
  {
    double latValue = Double.parseDouble(geoLocation.substring(0, 8));   
    double lngValue = Double.parseDouble(geoLocation.substring(11, 19));    
    LatLng geo = new LatLng(latValue, lngValue);
    return geo;
  }

  @Override
  public String toString()
  {
    return "Residence [geoLocation=" + geoLocation + ", eircode=" + eircode + ", rentalAmount=" + rentalAmount
        + ", numberBedrooms=" + numberBedrooms + ", numberBathrooms=" + numberBathrooms + ", residenceType="
        + residenceType + ", area=" + area + ", dateRegistered=" + dateRegistered + "]";
  }
  
}
