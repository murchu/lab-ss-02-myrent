package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import play.db.jpa.Model;

/**
 *  @file         Tenant.java
 *  @description
 *    Tenant db model representing tenant users registered with the site.
 *    
 *  @author       Damien Murphy
 *  @since        26 Jul 2016
 */
@Entity
public class Tenant extends Model
{     
  @OneToOne(mappedBy = "tenant")
  public Residence tenancy;
  
  public String firstName;
  public String lastName;  
  public String email;
  public String password;
  
  /**
   * Returns the tenant user that corresponds to the email submitted
   * 
   * @param email email of the tenant to search for
   * @return the tenant that corresponds to the email passed
   */
  public static Tenant findByEmail(String email)
  {
    return find("email", email).first();
  }
  
  /**
   * Sets a residence as the tenants tenancy
   * 
   * @param residence residence that is the new tenancy
   */
  public void registerTenancy(Residence residence)
  {
    this.tenancy = residence;
  }
  
  /**
   * Removes the current tenants tenancy
   * 
   */
  public void removeTenancy()
  {
    this.tenancy = null;
  }

  @Override
  public String toString()
  {
    return "Tenant [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", password=" + password
        + "]";
  }  
    
}
