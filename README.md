# MyRent #

### What is this repository ? ###

This repository is the completed code submission and web-app for the final assignment of the 2016 Summer School module of the [Higher Diploma in Science, Computer Science programme].

The assignment involves the implementation of a range of stories to further enhance the functionality of MyRent. MyRent is an imagined service site for the rental market which facilitates the registration of property and rental details by landlords and tenants, with a view to being able to offer a transparent view of the rental market at any given time.

Developed in Java using the Play MVC framework (v1.41)

Submitted: 20th August, 2016

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

[Higher Diploma in Science, Computer Science programme]: <https://www.wit.ie/courses/school/science/department_of_computing_maths_physics/higher-diploma-in-science-in-computer-science#tab=outline>
